﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Duplicates
{
    class Program
    {
        static void Main(string[] args)
        {

            string[] a = { "John", "Taylor", "John" }; // ➞ ["John", "Taylor"];
            int[] b = { 1, 0, 1, 0 }; // ➞ [1, 0]
            string[] c = { "The", "big", "cat" }; // ➞ ['The', 'big', 'cat']


            while (true)
            {
                Console.WriteLine("Type exit to leave program, or any key to progress:");
                string line = Console.ReadLine();
                var sortedWords = new HashSet<string>(a);
                foreach (string word in sortedWords)
                {
                    Console.Write(word + " ");
                }
                Console.Write(Environment.NewLine);

                var sortedNums = new HashSet<int>(b);
                foreach (int word in sortedNums)
                {
                    Console.Write(word + " ");
                }
                Console.Write(Environment.NewLine);

                var sortedWordsTwo = new HashSet<string>(c);
                foreach (string word in sortedWordsTwo)
                {
                    Console.Write(word + " ");
                }
                Console.Write(Environment.NewLine);





                if (line == "exit") // Check string
                {
                    break;
                }

            }


        }
    }
}
